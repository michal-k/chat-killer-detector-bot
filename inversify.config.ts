import "reflect-metadata";
import {ContainerProvider} from "./src/providers/container-provider";

let container = new ContainerProvider().getContainer();

export default container;
