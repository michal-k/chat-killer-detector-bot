require('dotenv').config()

require('source-map-support').install();
import container from "./inversify.config";

import {TYPES} from "./src/types";
import mongoose = require("mongoose");
import {ChatKillerDetectorBot} from "./src/chat-killer-detector-bot";

mongoose.connect(container.get(TYPES.MongodbUri), {useNewUrlParser: true}).then(() => {

    let bot: ChatKillerDetectorBot = container.get<ChatKillerDetectorBot>(TYPES.ChatKillerDetectorBot);
    bot.run()
}).catch(() => {
    console.log('ERROR: closing MongoDB connection.');
    return mongoose.connection.close();
})
