var findInFiles = require('find-in-files');
const save = require('save-file')
var read = require('read-file');

let existingTypes = require('../.types.json') || []

readFromEnv('.env.example')
    .then((result: string[]) => {
        return updateTypes(result.concat(existingTypes))
    }).then(() => {
    console.log('Done.')
})


function readFromEnv(fileName) {
    return new Promise((resolve, reject) => {
        read(fileName, 'utf8', (err, buffer) => {
            let result = [];
            buffer.split("\n").forEach((elem) => {
                let str = elem.replace('=', '');
                if (str != '') {
                    let toAdd = '';
                    str.toLowerCase().split('_').forEach((part) => {
                        toAdd += (part.charAt(0).toUpperCase() + part.slice(1))
                    })
                    result.push(toAdd);
                }
            });
            return resolve(result)
        })
    });
}

function updateTypes(existingTypes: string[]) {

    return findInFiles.find("export[ ]*class ([a-z,A-Z,0-9]*) .*{", 'src/', '.ts$')
        .then((results) => {
            for (var result in results) {
                var res = results[result];

                for (var m in res.matches) {
                    var match: string = res.matches[m];
                    var className = match
                        .replace(/.*class ([a-z,A-Z,0-9]*) .*{/, '$1')
                        .trim()

                    console.log(`${className}: found in '${result}'`);
                    existingTypes.push(className);
                }
            }

            return existingTypes
        }).then((types: string[]) => {
            types = types.sort()
            let symbolArray = [];
            types.forEach((className) => {
                symbolArray.push(`${className}: Symbol.for("${className}")`)
            })
            return symbolArray;
        }).then((symbolArray: string[]) => {
            let fileStr = "export const TYPES = {\n    ";
            fileStr += symbolArray.join(",\n    ");
            fileStr += "\n}";
            return save(fileStr, 'src/types.ts')
        })
}
