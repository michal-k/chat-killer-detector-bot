import {Guild, Message} from 'discord.js';
import {Logger} from "winston";
import * as winston from "winston";
import {inject, injectable} from "inversify";
import {TYPES} from "./types";
import {DiscordService} from "./services/discord/discord-service";
import {KillingMessagePeriodicChecker} from "./services/killing-message-periodic-checker";
import {CommandResolver} from "./commands/resolver/command-resolver";

@injectable()
export class ChatKillerDetectorBot {

    private logger: Logger;
    private discord: DiscordService;
    private checker: KillingMessagePeriodicChecker;
    private commandResolver: CommandResolver;

    // private messageController: KillingMessageController

    constructor(
        @inject(TYPES.Logger)  logger: winston.Logger,
        @inject(TYPES.DiscordService)  discord: DiscordService,
        @inject(TYPES.CommandResolver)  commandResolver: CommandResolver,
        @inject(TYPES.KillingMessagePeriodicChecker) checker: KillingMessagePeriodicChecker
    ) {
        this.logger = logger;
        this.discord = discord;
        this.commandResolver = commandResolver;
        this.checker = checker;
    }

    run() {
        this.registerListeners();
        this.logger.info('Before login');
        this.discord.login().then(() => {
            this.checker.start();
        })
    }

    private registerListeners() {
        this.logger.info('Calling on');
        this.discord.on('message', (message: Message) => {
            this.logger.info('Inside on');

            this.commandResolver.resolve(message);
            return null;
        });
    }
}
