import {DiscordMessageSender} from "../services/discord/discord-message-sender";
import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {PermissionsService} from "../services/permissions-service";
import {Message} from "discord.js";

@injectable()
export class CommandBase {
    protected messageSender: DiscordMessageSender;
    protected permissions: PermissionsService;

    constructor(
        @inject(TYPES.PermissionsService) permissions: PermissionsService,
        @inject(TYPES.DiscordMessageSender) messageSender: DiscordMessageSender
    ) {
        this.permissions = permissions
        this.messageSender = messageSender;
    }

    isUserAllowedToRun(message: Message): boolean {
        return this.permissions.isOwnerOrOverride(message);
    }
}
