import {TYPES} from "../types";
import {inject, injectable, LazyServiceIdentifer} from "inversify";
import {Message, TextChannel, User} from "discord.js";
import {CommandInterface} from "./interfaces/command-interface";
import {DiscordMessageSender} from "../services/discord/discord-message-sender";

@injectable()
export class HelpCommand implements CommandInterface {

    protected messageSender: DiscordMessageSender;
    private commands: CommandInterface[];

    constructor(
        @inject(TYPES.CommandsForHelp) commands: CommandInterface[],
        @inject(TYPES.DiscordMessageSender) messageSender: DiscordMessageSender,
    ) {
        this.commands = commands;
        this.messageSender = messageSender;
    }

    public run(message: Message, subCommand) {

        let msg = this.getMessageResponse(subCommand);
        if (!msg) return;

        return this.messageSender.sendMessageToChannel(message.channel as TextChannel, msg)
    }

    private getMessageResponse(subCommand) {
        if (subCommand == '') {
            return this.getGlobalHelp();
        }

        for (let cmd of this.commands) {
            if (subCommand == cmd.getName()) {
                let msg = this.getBasicCmdDesc(cmd);
                msg += `Użycie: ${cmd.getName()} ${cmd.getParameterNames()}\n`
                let parameterExamples = cmd.getParameterExamples();
                if (parameterExamples.length > 0) {
                    msg += 'Przykładowe wywołania:\n';
                }
                for (let param of parameterExamples) {
                    msg += `${param}\n`;
                }
                return msg;
            }
        }

        console.log("nothing found in help")
        return null;
    }

    private getGlobalHelp() {
        let msg = ''
        msg += 'Lista komend:\n';
        for (let cmd of this.commands) {
            msg += this.getBasicCmdDesc(cmd)
        }
        msg += `Użyj help <nazwa_komendy> aby uzyskać więcej informacji.\n`;
        return msg;
    }

    private getBasicCmdDesc(cmd) {
        return `${cmd.getName()} - ${cmd.getDescription()}\n`;
    }

    getDescription(): string {
        return "Wyświetlenie pomocy.";
    }

    getName(): string {
        return "help";
    }

    isUserAllowedToRun(message: Message): boolean {
        return true;
    }

    getParameterExamples(): string[] {
        return [];
    }

    getParameterNames(): string {
        return "";
    }
}
