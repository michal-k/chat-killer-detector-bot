import {Message, User} from "discord.js";

export interface CommandInterface {
    run(message: Message, command): Promise<any>;

    getName(): string;

    getDescription(): string;

    getParameterExamples(): string[];

    getParameterNames(): string;

    isUserAllowedToRun(message: Message): boolean;
}
