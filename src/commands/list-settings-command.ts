import {injectable} from "inversify";
import {Message, TextChannel} from "discord.js";
import {SettingsCommandBase} from "./settings-command-base";
import {IServerSettingModel} from "../models/server-setting-model";
import {CommandInterface} from "./interfaces/command-interface";

@injectable()
export class ListSettingsCommand extends SettingsCommandBase implements CommandInterface {

    public run(message: Message, command) {
        return this.repository.getSettings(message.guild.id).then((settings: IServerSettingModel) => {
            let msg = this.getMsg(settings);
            return this.messageSender.sendMessageToChannel(message.channel as TextChannel, msg)
        })
    }

    private getMsg(settings: IServerSettingModel) {
        if (!settings) {
            return 'Brak ustawień.'
        }

        let msg = `Oto ustawienia:\n`;
        let opts = [
            'defaultTime',
            'prefix',
            'setAllowedRoleIds',
            'channelNames',
            'notifyUsersIds'];
        opts.forEach((setting) => {
            let value = settings[setting] || 'nie ustawiono'
            msg += ` ${setting}: ${value}\n`
        })
        return msg;
    }

    getDescription(): string {
        return "Wyświetlanie ustawień serwera.";
    }

    getName(): string {
        return "list-settings";
    }

    isUserAllowedToRun(message: Message): boolean {
        return true;
    }

    getParameterExamples(): string[] {
        return [];
    }

    getParameterNames(): string {
        return "";
    }
}
