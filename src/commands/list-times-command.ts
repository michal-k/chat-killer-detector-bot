import {inject, injectable} from "inversify";
import {Message, TextChannel, User} from "discord.js";
import {IServerTimeLimitModel} from "../models/server-time-limit-model";
import {TimeCommandBase} from "./time-command-base";
import {CommandInterface} from "./interfaces/command-interface";

let moment = require("moment");

@injectable()
export class ListTimesCommand extends TimeCommandBase implements CommandInterface {

    private _timezoneOffset = 0

    public setTimezoneOffset(valueInMinutes) {
        this._timezoneOffset = valueInMinutes;
    }

    public run(message: Message, command: string) {
        let serverId = message.guild.id;
        return this.defaultTimeProvider.getDefaultTimeLimit(serverId).then((defaultTime) => {

            return this.repository.getUTCTimeLimits(serverId)
                .then((timeLimits: IServerTimeLimitModel[]) => {
                    let msg = this.prepareMsg(defaultTime, timeLimits);

                    return this.messageSender.sendMessageToChannel(message.channel as TextChannel, msg)
                })
        })

    }

    private prepareMsg(defaultTime: number, timeLimits: IServerTimeLimitModel[]) {
        let msg = `Oto czasy działania bota (domyślny czas: ${this.getTimeStr(defaultTime)}):\n`;
        let times = {};

        timeLimits.forEach((limit: IServerTimeLimitModel) => {
            let m = moment().utcOffset(-this._timezoneOffset);
            m.hours(limit.utcHour);
            let local = m.local();
            times[local.hours()] = limit;
        })

        console.log('times', times)

        for (let i = 0; i < 24; i++) {
            let timeLimit = defaultTime;
            let limit = times[i];
            if (!!limit) {
                timeLimit = limit.timeLimit;
            }
            let timestr = this.getTimeStr(timeLimit);

            msg += ` Godz. ${i}:00-${i}:59 – ${timestr}\n`
        }

        msg += "Dodatkowo, bot sprawdza czas co kilka minut, więc powyższe czasy mogą być minimalnie wyższe."
        return msg;
    }

    private getTimeStr(timeLimit: number) {
        if (timeLimit === null) {
            return 'nie ustawiono'
        }
        if (timeLimit < 0) {
            return `wyłączony`;
        }
        let timestr = `${timeLimit} min`;
        return timestr;
    }

    getDescription(): string {
        return "Wyświetlanie czasów, po jakich bot się uaktywnia.";
    }

    getName(): string {
        return "list-times";
    }

    isUserAllowedToRun(message: Message): boolean {
        return true;
    }

    getParameterExamples(): string[] {
        return [];
    }

    getParameterNames(): string {
        return "";
    }
}
