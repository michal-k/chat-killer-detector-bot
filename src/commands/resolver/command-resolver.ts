import {TYPES} from "../../types";
import {inject, injectable} from "inversify";
import {Message} from "discord.js";
import {GuildsProvider} from "../../providers/guilds-provider";
import {CommandInterface} from "../interfaces/command-interface";
import {PrefixProvider} from "../../providers/prefix-provider";

@injectable()
export class CommandResolver {

    private commands: CommandInterface[]
    private guildsProvider: GuildsProvider
    private prefixProvider: PrefixProvider;
    private overrideUserId: string;

    constructor(
        @inject(TYPES.Commands) commands: CommandInterface[],
        @inject(TYPES.GuildsProvider)  guildsProvider: GuildsProvider,
        @inject(TYPES.PrefixProvider)  prefixProvider: PrefixProvider,
        @inject(TYPES.OverrideUserId) overrideUserId: string

    ) {
        this.commands = commands;
        this.guildsProvider = guildsProvider;
        this.prefixProvider = prefixProvider;
        this.overrideUserId = overrideUserId
    }

    public async resolve(message: Message) {
        console.log('got message: ', message.content)
        if (message.author.bot) {
            return;
        }

        let isDirectMessage = message.guild == null;
        let prefix = await this.prefixProvider.findPrefixUsed(message);
        let isCommand = message.content.startsWith(prefix);

        if (isCommand && isDirectMessage) {
            message.reply("Wiadomości prywatne obecnie są nieobsługiwane.")
            console.log("Sent direct message probably, exiting")
            return;
        }

        if (!this.shouldContinue(message, isCommand)) {
            console.log('shouldContinue = false, exiting.')
            return;
        }

        let tokens = message.content.split(' ');
        if (tokens.length > 1) {
            let commandName = tokens[1];
            let remaining = message.content.replace(`${prefix} ${commandName}`, '').trim();

            console.log('command', commandName)
            for (let command of this.commands) {
                if (command.getName() == commandName) {
                    if (!command.isUserAllowedToRun(message)) {
                        message.reply('Nie przyznano uprawnień do komendy.')
                        return;
                    }
                    command.run(message, remaining);
                    return;
                }
            }

        }
    }

    private shouldContinue(message: Message, isCommand: boolean) {
        if (!isCommand) {
            console.log('Not a command, will exit')
            return false
        }

        return this.checkIsGuildOk(message);


    }

    private checkIsGuildOk(message: Message) {
        for (let guild of this.guildsProvider.getGuilds()) {
            if (guild.id == message.guild.id) {
                return true;
            }
        }
        console.log('Not ok guild, will exit')
        return false;
    }
}
