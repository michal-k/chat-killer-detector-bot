import {injectable} from "inversify";
import {Message, TextChannel, User} from "discord.js";
import {TimeCommandBase} from "./time-command-base";
import {CommandInterface} from "./interfaces/command-interface";

@injectable()
export class SaveTimeCommand extends TimeCommandBase implements CommandInterface {
    public run(message: Message, command: string) {
        console.log('command', command)
        let values = command.split(' ');
        let hour, timeLimit;
        try {
            hour = Number.parseInt(values[0]);
            timeLimit = Number.parseInt(values[1]);

            console.log('utcHour', hour)
            console.log('timeLimit', timeLimit)

            let date = new Date()
            date.setHours(hour);
            return this.repository.setTimeLimit(message.guild.id, timeLimit, date.getUTCHours()).then(() => {
                return this.messageSender.sendMessageToChannel(message.channel as TextChannel,
                    `Ustawiono czas ${timeLimit > 0 ? timeLimit + ' min' : '<wyłączony>'} dla godziny ${hour}.`)
            })
        } catch (e) {
            this.messageSender.sendMessageToChannel(message.channel as TextChannel, e.message)
            return;
        }

    }

    getDescription(): string {
        return "Ustawienie czasu działania bota.";
    }

    getName(): string {
        return "set-time";
    }

    getParameterExamples(): string[] {
        return [
            "10 30"
        ];
    }

    getParameterNames(): string {
        return "<godzina> <liczba_minut>";
    }
}
