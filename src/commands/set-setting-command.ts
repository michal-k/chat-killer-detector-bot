import {injectable} from "inversify";
import {Message, TextChannel, User} from "discord.js";
import {SettingsCommandBase} from "./settings-command-base";
import {CommandInterface} from "./interfaces/command-interface";

@injectable()
export class SetSettingCommand extends SettingsCommandBase implements CommandInterface {

    public run(message: Message, command: string) {
        console.log('command', command)
        let values = command.split(' ');
        let setting, value;
        try {
            setting = (values[0]);
            value = (values[1]);

            console.log('setting', setting)
            console.log('value', value)

            let date = new Date()
            let toSet = {};
            toSet[setting] = value;
            return this.repository.setSettings(message.guild.id, toSet).then(() => {
                return this.messageSender.sendMessageToChannel(message.channel as TextChannel,
                    `Ustawiono ${value} na ${setting}.`)
            })
        } catch (e) {
            this.messageSender.sendMessageToChannel(message.channel as TextChannel, e.message)
            return;
        }

    }

    getDescription(): string {
        return "Ustawienie parametrów serwera.";
    }

    getName(): string {
        return "set-setting";
    }

    getParameterExamples(): string[] {
        return [
            "defaultTime 30",
            "prefix +pr",
            "setAllowedRoleIds '1,2,3'",
            // "channelNames 'a,b,c'",
            // "notifyUsersIds '1,2,3'"
        ];
    }

    getParameterNames(): string {
        return "<nazwa> <wartość>";
    }
}
