import {DiscordMessageSender} from "../services/discord/discord-message-sender";
import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {CommandBase} from "./command-base";
import {PermissionsService} from "../services/permissions-service";
import {IServerSettingRepository} from "../repositories/server-setting-repository";

@injectable()
export class SettingsCommandBase extends CommandBase {
    protected repository: IServerSettingRepository;

    constructor(
        @inject(TYPES.PermissionsService) permissions: PermissionsService,
        @inject(TYPES.DiscordMessageSender) messageSender: DiscordMessageSender,
        @inject(TYPES.ServerSettingRepository) repository: IServerSettingRepository,
    ) {
        super(permissions, messageSender)
        this.repository = repository;
    }
}
