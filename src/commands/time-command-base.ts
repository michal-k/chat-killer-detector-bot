import {inject, injectable} from "inversify";
import {DiscordMessageSender} from "../services/discord/discord-message-sender";
import {TYPES} from "../types";
import {PermissionsService} from "../services/permissions-service";
import {CommandBase} from "./command-base";
import {DefaultTimeLimitProvider} from "../providers/default-time-limit-provider";
import {IServerTimeLimitRepository} from "../repositories/server-time-limit-repository";

@injectable()
export class TimeCommandBase extends CommandBase {
    protected defaultTimeProvider: DefaultTimeLimitProvider;
    protected repository: IServerTimeLimitRepository;

    constructor(
        @inject(TYPES.PermissionsService) permissions: PermissionsService,
        @inject(TYPES.DiscordMessageSender) messageSender: DiscordMessageSender,
        @inject(TYPES.ServerTimeLimitRepository) repository: IServerTimeLimitRepository,
        @inject(TYPES.DefaultTimeLimitProvider) defaultTimeProvider: DefaultTimeLimitProvider
    ) {
        super(permissions, messageSender)
        this.repository = repository;
        this.defaultTimeProvider = defaultTimeProvider;
    }
}
