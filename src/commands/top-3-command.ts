import {inject, injectable} from "inversify";
import {GuildChannel, Message} from "discord.js";
import {CommandInterface} from "./interfaces/command-interface";
import {TYPES} from "../types";
import {PermissionsService} from "../services/permissions-service";
import {DiscordMessageSender} from "../services/discord/discord-message-sender";
import {CommandBase} from "./command-base";
import {KillingMessageController} from "../services/messages/killing-message-controller";

@injectable()
export class Top3Command extends CommandBase implements CommandInterface {
    private sender: KillingMessageController;

    constructor(
        @inject(TYPES.PermissionsService) permissions: PermissionsService,
        @inject(TYPES.DiscordMessageSender) messageSender: DiscordMessageSender,
        @inject(TYPES.KillingMessageController) sender: KillingMessageController
    ) {
        super(permissions, messageSender)
        this.sender = sender;
    }

    public run(message: Message, command): Promise<any> {
        console.log('command', command)

        return this.sender.sendTop3ForChannel(message.channel as GuildChannel);
    }

    getDescription(): string {
        return "Top 3 osób, które zabiły czat.";
    }

    getName(): string {
        return "top-3";
    }

    getParameterExamples(): string[] {
        return [];
    }

    getParameterNames(): string {
        return "";
    }
}
