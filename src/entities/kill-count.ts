export interface IKillCount {
    serverId: string,
    userId: string,
    count: number
    month: number,
    year: number,
}
