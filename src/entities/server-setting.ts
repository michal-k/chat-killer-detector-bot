export interface IServerSetting {
    defaultTime?: number,
    prefix?: string,
    setAllowedRoleIds?: string,
    channelNames?: string,
    notifyUsersIds?: string
}
