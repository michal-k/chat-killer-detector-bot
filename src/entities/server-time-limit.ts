export interface IServerTimeLimit {
    serverId: string,
    utcHour: number,
    timeLimit: number
}
