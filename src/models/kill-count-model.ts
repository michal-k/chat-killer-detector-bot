import mongoose = require("mongoose");
import {IKillCount} from "../entities/kill-count";

export interface IKillCountModel extends IKillCount, mongoose.Document {
}
