import mongoose = require("mongoose");
import {IServerSetting} from "../entities/server-setting";

export interface IServerSettingModel extends IServerSetting, mongoose.Document {
}
