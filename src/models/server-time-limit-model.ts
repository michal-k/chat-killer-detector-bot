import mongoose = require("mongoose");
import {IServerTimeLimit} from "../entities/server-time-limit";

export interface IServerTimeLimitModel extends IServerTimeLimit, mongoose.Document {
}
