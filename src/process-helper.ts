export class ProcessHelper {
    public getTruthyValue(envKey: string, defaultValue: boolean = false) {
        if (process.env[envKey] === undefined) {
            return defaultValue;
        }
        return ["true", "1", "on"].indexOf(process.env[envKey].toLowerCase()) >= 0;
    }
}

