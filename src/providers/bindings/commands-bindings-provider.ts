import {ListTimesCommand} from "../../commands/list-times-command";
import {SaveTimeCommand} from "../../commands/save-time-command";
import {SetSettingCommand} from "../../commands/set-setting-command";
import {ListSettingsCommand} from "../../commands/list-settings-command";
import {TYPES} from "../../types";
import {ContainerBindingsProvider} from "./container-bindings-provider";
import {CommandInterface} from "../../commands/interfaces/command-interface";
import {HelpCommand} from "../../commands/help-command";
import {Top3Command} from "../../commands/top-3-command";

export class CommandsBindingsProvider extends ContainerBindingsProvider implements ContainerBindingsProvider {
    public bind() {
        this.container.bind<Top3Command>(TYPES.Top3Command).to(Top3Command).inSingletonScope();
        this.container.bind<ListTimesCommand>(TYPES.ListTimesCommand).to(ListTimesCommand).inSingletonScope();
        this.container.bind<SaveTimeCommand>(TYPES.SaveTimeCommand).to(SaveTimeCommand).inSingletonScope();
        this.container.bind<SetSettingCommand>(TYPES.SetSettingCommand).to(SetSettingCommand).inSingletonScope();
        this.container.bind<ListSettingsCommand>(TYPES.ListSettingsCommand).to(ListSettingsCommand).inSingletonScope();
        this.container.bind<HelpCommand>(TYPES.HelpCommand).to(HelpCommand).inSingletonScope();

        let commandsWithoutHelp: CommandInterface[] = [
            this.container.get<ListTimesCommand>(TYPES.ListTimesCommand),
            this.container.get<SaveTimeCommand>(TYPES.SaveTimeCommand),
            this.container.get<SetSettingCommand>(TYPES.SetSettingCommand),
            this.container.get<ListSettingsCommand>(TYPES.ListSettingsCommand),
            this.container.get<Top3Command>(TYPES.Top3Command),
        ];
        this.container.bind<CommandInterface[]>(TYPES.CommandsForHelp).toConstantValue(commandsWithoutHelp);
        commandsWithoutHelp.push(this.container.get<HelpCommand>(TYPES.HelpCommand))
        this.container.bind<CommandInterface[]>(TYPES.Commands).toConstantValue(commandsWithoutHelp);
    }
}
