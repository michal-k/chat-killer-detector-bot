import {Container} from "inversify";

export interface ContainerBindingsProvider {
    bind();
}

export abstract class ContainerBindingsProvider {
    protected container: Container;

    constructor(container: Container) {
        this.container = container;
    }
}
