import {TYPES} from "../../types";
import {ProcessHelper} from "../../process-helper";
import {ContainerBindingsProvider} from "./container-bindings-provider";

export class EnvBindingsProvider extends ContainerBindingsProvider implements ContainerBindingsProvider {
    public bind() {

        let pH = new ProcessHelper();

        this.container.bind<string>(TYPES.Token).toConstantValue(process.env.TOKEN);
        this.container.bind<string>(TYPES.MongodbUri).toConstantValue(process.env.MONGODB_URI || '');
        this.container.bind<string>(TYPES.MongodbTestUri).toConstantValue(process.env.MONGODB_TEST_URI || '');
        this.container.bind<string>(TYPES.NotifyUsers).toConstantValue(process.env.NOTIFY_USERS || '');
        this.container.bind<string>(TYPES.ChannelNames).toConstantValue(process.env.CHANNEL_NAMES);
        this.container.bind<string>(TYPES.TestGuildIds).toConstantValue(process.env.TEST_GUILD_IDS || '');
        this.container.bind<string>(TYPES.ExcludedGuildIds).toConstantValue(process.env.EXCLUDED_GUILD_IDS || '');
        this.container.bind<string>(TYPES.OverrideUserId).toConstantValue(process.env.OVERRIDE_USER_ID || '');
        this.container.bind<boolean>(TYPES.SelfTrigger).toDynamicValue(() => {
            return pH.getTruthyValue("SELF_TRIGGER", false);
        });

        this.container.bind<boolean>(TYPES.TestMode).toDynamicValue(() => {
            return pH.getTruthyValue("TEST_MODE", true);
        });

        this.container.bind<number>(TYPES.TimeLimit).toDynamicValue(() => {
            return (process.env.TIME_LIMIT !== undefined) ? Number.parseInt(process.env.TIME_LIMIT) : 10;
        });

        this.container.bind<string>(TYPES.Cron).toDynamicValue(() => {
            return process.env.CRON || null;
        });

        this.container.bind<boolean>(TYPES.Mention).toDynamicValue(() => {
            return pH.getTruthyValue("MENTION", true);
        });
    }
}
