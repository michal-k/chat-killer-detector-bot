import {TYPES} from "../../types";
import {
    IServerTimeLimitRepository,
    IServerTimeLimitRepositoryFunctions
} from "../../repositories/server-time-limit-repository";
import {IKillCountRepository} from "../../repositories/kill-count-repository";
import * as mongoose from "mongoose";
import {Schema} from "mongoose";
import {
    IServerSettingRepository,
    IServerSettingRepositoryFunctions
} from "../../repositories/server-setting-repository";
import {ServerTimeLimitSchema} from "../../schemas/server-time-limit-schema";
import {ServerSettingSchema} from "../../schemas/server-setting-schema";
import {KillCountSchema} from "../../schemas/kill-count-schema";
import {ContainerBindingsProvider} from "./container-bindings-provider";

export class MongooseBindingsProvider extends ContainerBindingsProvider implements ContainerBindingsProvider {
    public bind() {
        this.bindMongooseSchemas();
        this.bindMongooseModels();
    }

    private bindMongooseSchemas() {
        this.bindSchema(TYPES.KillCountSchema, KillCountSchema);
        this.bindSchema(TYPES.ServerTimeLimitSchema, ServerTimeLimitSchema);
        this.bindSchema(TYPES.ServerSettingSchema, ServerSettingSchema);
    }

    private bindSchema(serviceIdentifier, className) {
        this.container.bind<Schema>(serviceIdentifier).toDynamicValue(() => {
            return new mongoose.Schema(className);
        }).inSingletonScope();
    }

    private bindMongooseModels() {
        this.bindModel<IKillCountRepository>(
            TYPES.KillCountRepository, 'KillCount', TYPES.KillCountSchema
        );
        this.bindModel<IServerTimeLimitRepository>(
            TYPES.ServerTimeLimitRepository, 'ServerTimeLimit', TYPES.ServerTimeLimitSchema, IServerTimeLimitRepositoryFunctions.apply
        );
        this.bindModel<IServerSettingRepository>(
            TYPES.ServerSettingRepository, 'ServerSetting', TYPES.ServerSettingSchema, IServerSettingRepositoryFunctions.apply
        );
    }

    private bindModel<T extends mongoose.Model<mongoose.Document>>(
        repoIdentifier: symbol, modelName: string, schemaIdentifier: symbol, functions?: any
    ) {
        this.container.bind<mongoose.Model<mongoose.Document>>(repoIdentifier).toDynamicValue(() => {
            let model = this.getModel(modelName, schemaIdentifier);
            if (functions) {
                model = functions(model);
            }
            return model;
        }).inSingletonScope();
    }

    private getModel(modelName: string, schemaIdentifier: symbol) {
        if (mongoose.modelNames().indexOf(modelName) === -1) {
            return mongoose.model(modelName, this.container.get<Schema>(schemaIdentifier));
        }
        return mongoose.connection.model(modelName);
    }
}
