import {DiscordService} from "../../services/discord/discord-service";
import {CommandResolver} from "../../commands/resolver/command-resolver";
import {TYPES} from "../../types";
import {KillsWithMembersProvider} from "../../services/messages/kills-with-members-provider";
import {KillCountService} from "../../services/kill-count-service";
import {MessagesSender} from "../../services/messages/messages-sender";
import {MessagesDataProvider} from "../../services/messages/messages-data-provider";
import {KillingMessageController} from "../../services/messages/killing-message-controller";
import {KillTimeChecker} from "../../services/kill-time-checker";
import {KillingMessagePeriodicChecker} from "../../services/killing-message-periodic-checker";
import {DiscordMessageSender} from "../../services/discord/discord-message-sender";
import {LoggerService} from "../../services/log/logger-service";
import {KillingMessageResolver} from "../../services/messages/killing-message-resolver";
import {ContainerBindingsProvider} from "./container-bindings-provider";
import {PermissionsService} from "../../services/permissions-service";
import {MessageContentsProvider} from "../../services/messages/message-contents-provider";

export class ServicesBindingsProvider extends ContainerBindingsProvider implements ContainerBindingsProvider {
    public bind() {
        this.container.bind<KillingMessageController>(TYPES.KillingMessageController).to(KillingMessageController).inSingletonScope();
        this.container.bind<KillingMessageResolver>(TYPES.KillingMessageResolver).to(KillingMessageResolver).inSingletonScope();
        this.container.bind<KillCountService>(TYPES.KillCountService).to(KillCountService).inSingletonScope();
        this.container.bind<KillTimeChecker>(TYPES.KillTimeChecker).to(KillTimeChecker).inSingletonScope();
        this.container.bind<KillsWithMembersProvider>(TYPES.KillsWithMembersProvider).to(KillsWithMembersProvider).inSingletonScope();
        this.container.bind<MessagesSender>(TYPES.MessagesSender).to(MessagesSender).inSingletonScope();
        this.container.bind<DiscordMessageSender>(TYPES.DiscordMessageSender).to(DiscordMessageSender).inSingletonScope();
        this.container.bind<DiscordService>(TYPES.DiscordService).to(DiscordService).inSingletonScope();
        this.container.bind<MessagesDataProvider>(TYPES.MessagesDataProvider).to(MessagesDataProvider).inSingletonScope();
        this.container.bind<MessageContentsProvider>(TYPES.MessageContentsProvider).to(MessageContentsProvider).inSingletonScope();
        this.container.bind<KillingMessagePeriodicChecker>(TYPES.KillingMessagePeriodicChecker).to(KillingMessagePeriodicChecker).inSingletonScope();
        this.container.bind<LoggerService>(TYPES.LoggerService).to(LoggerService).inSingletonScope();
        this.container.bind<CommandResolver>(TYPES.CommandResolver).to(CommandResolver).inSingletonScope();
        this.container.bind<PermissionsService>(TYPES.PermissionsService).to(PermissionsService).inSingletonScope();

    }
}
