import {TYPES} from "../types";
import {Container} from "inversify";
import {GuildsProvider} from "./guilds-provider";
import {EnvBindingsProvider} from "./bindings/env-bindings-provider";
import {Client} from "discord.js";
import {MongooseBindingsProvider} from "./bindings/mongoose-bindings-provider";
import {ServicesBindingsProvider} from "./bindings/services-bindings-provider";
import {TimeLimitProvider} from "./time-limit-provider";
import {Logger} from "winston";
import {CommandsBindingsProvider} from "./bindings/commands-bindings-provider";
import {PrefixProvider} from "./prefix-provider";
import {DefaultTimeLimitProvider} from "./default-time-limit-provider";
import {ChatKillerDetectorBot} from "../chat-killer-detector-bot";

export class ContainerProvider {

    public getContainer() {

        let container = new Container();

        this.bindThirdPartySingletons(container);
        this.bindProviders(container);

        new EnvBindingsProvider(container).bind();
        new MongooseBindingsProvider(container).bind();
        new ServicesBindingsProvider(container).bind();
        new CommandsBindingsProvider(container).bind();

        this.bindBot(container);

        return container;
    }

    private bindProviders(container: Container) {
        container.bind<TimeLimitProvider>(TYPES.TimeLimitProvider).to(TimeLimitProvider).inSingletonScope();
        container.bind<GuildsProvider>(TYPES.GuildsProvider).to(GuildsProvider).inSingletonScope();
        container.bind<PrefixProvider>(TYPES.PrefixProvider).to(PrefixProvider).inSingletonScope();
        container.bind<DefaultTimeLimitProvider>(TYPES.DefaultTimeLimitProvider).to(DefaultTimeLimitProvider).inSingletonScope();


    }

    private bindBot(container: Container) {
        container.bind<ChatKillerDetectorBot>(TYPES.ChatKillerDetectorBot).to(ChatKillerDetectorBot).inSingletonScope();
    }

    private bindThirdPartySingletons(container: Container) {
        let logger = require('winston');
        container.bind<Logger>(TYPES.Logger).toDynamicValue(() => logger).inSingletonScope();
        container.bind<Client>(TYPES.Client).toDynamicValue(() => new Client).inSingletonScope();
    }
}
