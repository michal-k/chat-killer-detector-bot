import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {IServerSetting} from "../entities/server-setting";
import {IServerSettingRepository} from "../repositories/server-setting-repository";

@injectable()
export class DefaultTimeLimitProvider {
    private readonly timeLimit: number;
    private repository: IServerSettingRepository;

    constructor(
        @inject(TYPES.ServerSettingRepository) repository: IServerSettingRepository,
        @inject(TYPES.TimeLimit) timeLimit: number
    ) {
        this.repository = repository;
        this.timeLimit = timeLimit;
    }

    public getDefaultTimeLimit(serverId: string): Promise<number> {
        return this.repository.getSettings(serverId).then((settings: IServerSetting) => {
            if (settings && settings.defaultTime) {
                return settings.defaultTime
            }
            return this.timeLimit
        })
    }

}
