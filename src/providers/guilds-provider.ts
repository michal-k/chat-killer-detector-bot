import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {DiscordService} from "../services/discord/discord-service";
import {Guild} from "discord.js";
import {LoggerService} from "../services/log/logger-service";

@injectable()
export class GuildsProvider {
    private readonly testGuildIds: string[];
    private discord: DiscordService;
    private excludedGuildIds: string[];
    private readonly testMode: boolean;
    private logger: LoggerService;

    constructor(
        @inject(TYPES.DiscordService) discord: DiscordService,
        @inject(TYPES.LoggerService) logger: LoggerService,
        @inject(TYPES.TestGuildIds) testGuildIds: string,
        @inject(TYPES.ExcludedGuildIds) excludedGuildIds: string,
        @inject(TYPES.TestMode) testMode: boolean
    ) {
        this.discord = discord;
        this.logger = logger;
        this.testGuildIds = testGuildIds.split(",");
        this.excludedGuildIds = excludedGuildIds.split(",");
        this.testMode = testMode;
    }

    public getGuilds(): Guild[] {
        let guilds = []
        this.logger.info('TESTMODE' + this.testMode)
        this.discord.getCurrentGuilds().forEach((guild: Guild) => {
            if (this.testMode) {
                let isTestGuild = (this.testGuildIds.indexOf(guild.id) >= 0);
                if (!isTestGuild) {
                    this.logger.info(`GUILD: ${guild.name}(${guild.id}) not a test guild, excluding.`)
                    return;
                }
            } else {
                let isExcludedGuild = this.excludedGuildIds.indexOf(guild.id) >= 0;
                if (isExcludedGuild) {
                    this.logger.info(`GUILD: ${guild.name}(${guild.id}) excluded.`)
                    return;
                }
            }

            guilds.push(guild);
        });

        return guilds;
    }
}
