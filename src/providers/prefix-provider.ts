import {TYPES} from "../types";
import {inject, injectable} from "inversify";
import {Message} from "discord.js";
import {IServerSettingModel} from "../models/server-setting-model";
import {IServerSettingRepository} from "../repositories/server-setting-repository";

@injectable()
export class PrefixProvider {
    private repository: IServerSettingRepository;

    constructor(
        @inject(TYPES.ServerSettingRepository) repository: IServerSettingRepository,
    ) {
        this.repository = repository;
    }

    public findPrefixUsed(message: Message): Promise<string> {
        return this.repository.getSettings(message.guild.id).then((settings: IServerSettingModel) => {
            let defaultPrefix = '!zc';
            if (settings && settings.prefix) {
                defaultPrefix = settings.prefix;
            }
            let mentionPrefix = `<@${message.client.user.id}>`;
            let strings = message.content.split(' ');
            for (let prefix of [defaultPrefix, mentionPrefix]) {
                if (strings[0] && strings[0] == prefix) {
                    // console.log('prefix', prefix)
                    return prefix
                }
            }
        })
    }
}
