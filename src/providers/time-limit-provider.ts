import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {IServerTimeLimit} from "../entities/server-time-limit";
import {DefaultTimeLimitProvider} from "./default-time-limit-provider";
import {IServerTimeLimitRepository} from "../repositories/server-time-limit-repository";

@injectable()
export class TimeLimitProvider {
    private repository: IServerTimeLimitRepository;
    private defaultTimeProvider: DefaultTimeLimitProvider;

    constructor(
        @inject(TYPES.ServerTimeLimitRepository) repository: IServerTimeLimitRepository,
        @inject(TYPES.DefaultTimeLimitProvider) defaultTimeLimitProvider: DefaultTimeLimitProvider,
    ) {
        this.repository = repository;
        this.defaultTimeProvider = defaultTimeLimitProvider;
    }

    public getTimeLimit(serverId: string, localDate: Date): Promise<number> {
        return this.repository.getTimeLimitForUtcHour(serverId, localDate.getUTCHours())
            .then((timeLimit: IServerTimeLimit) => {
                if (timeLimit != null) {
                    return timeLimit.timeLimit
                }

                return this.defaultTimeProvider.getDefaultTimeLimit(serverId)
            })
    }
}
