import {Model} from "mongoose";
import {IKillCountModel} from "../models/kill-count-model";

export interface IKillCountRepository extends Model<IKillCountModel, {}> {

}
