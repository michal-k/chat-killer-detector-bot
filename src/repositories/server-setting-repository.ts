import {Model} from "mongoose";
import {IServerSettingModel} from "../models/server-setting-model";
import {IServerSetting} from "../entities/server-setting";

export interface IServerSettingRepository extends Model<IServerSettingModel, {}>, IServerSettingRepositoryFunctions {
}

export class IServerSettingRepositoryFunctions {
    static apply(model) {
        model.setSettings = Functions.prototype.setSettings
        model.getSettings = Functions.prototype.getSettings
        return model
    }
}


export interface IServerSettingRepositoryFunctions {
    setSettings(serverId: string, settings: IServerSetting);

    getSettings(serverId: string): Promise<IServerSetting>;
}


class Functions implements IServerSettingRepositoryFunctions {
    [x: string]: any;
    public setSettings(serverId: string, settings: IServerSetting) {
        return this.updateOne({
            serverId: serverId
        }, settings, {upsert: true}).exec()
    }

    public getSettings(serverId: string): Promise<IServerSetting> {
        return this.findOne({
            serverId: serverId
        }).exec()
    }
}
