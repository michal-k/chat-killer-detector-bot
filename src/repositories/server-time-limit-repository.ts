import * as mongoose from "mongoose";
import {Model} from "mongoose";
import {IServerTimeLimitModel} from "../models/server-time-limit-model";

export interface IServerTimeLimitRepository extends Model<IServerTimeLimitModel, {}>, IServerTimeLimitRepositoryFunctions {

}

export class IServerTimeLimitRepositoryFunctions {
    static apply(model) {
        model.setTimeLimit = Functions.prototype.setTimeLimit
        model.getTimeLimitForUtcHour = Functions.prototype.getTimeLimitForUtcHour
        model.getUTCTimeLimits = Functions.prototype.getUTCTimeLimits
        return model
    }
}


export interface IServerTimeLimitRepositoryFunctions {

    setTimeLimit(serverId: string, timeLimit: number, utcHour: number);

    getTimeLimitForUtcHour(serverId: string, utcHour: number): Promise<IServerTimeLimitModel>;

    getUTCTimeLimits(serverId: string): Promise<IServerTimeLimitModel[]>;
}


class Functions implements IServerTimeLimitRepositoryFunctions {
    [x: string]: any;

    public setTimeLimit(serverId: string, timeLimit: number, utcHour: number) {
        return this.updateOne({
            serverId: serverId,
            utcHour: utcHour
        }, {timeLimit: timeLimit}, {upsert: true}).exec()
    }

    getTimeLimitForUtcHour(serverId: string, utcHour: number): Promise<IServerTimeLimitModel> {
        return this.findOne({
            serverId: serverId,
            utcHour: utcHour,
        }).exec()
    }

    getUTCTimeLimits(serverId: string): Promise<IServerTimeLimitModel[]> {
        return this.find({
            serverId: serverId,
        }).sort({utcHour: 'asc'}).exec()
    }
}
