export const KillCountSchema = {
    serverId: String,
    userId: String,
    month: Number,
    year: Number,
    count: Number
};
