export const ServerSettingSchema = {
    serverId: String,
    defaultTime: String,
    prefix: String,
    setAllowedRoleIds: String,
    channelNames: String,
    notifyUsersIds: String
};

