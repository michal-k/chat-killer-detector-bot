export const ServerTimeLimitSchema = {
    serverId: String,
    utcHour: Number,
    timeLimit: Number
};
