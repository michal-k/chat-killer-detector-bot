import {TextChannel, Message, User} from "discord.js";
import {injectable} from "inversify";

@injectable()
export class DiscordMessageSender {

    public sendMessageToChannel(channel: TextChannel, msg: string, embed: boolean = true): Promise<any> {
        if (embed) {
            return channel.send('', this.getEmbedTemplate(msg))
        }
        channel.send(msg)
    }

    public sendMessageToMultipleUsers(users: User[], msg: string): Promise<any[]> {
        let promises = [];

        users.forEach((userToNotify: User) => {
            promises.push(userToNotify.send(msg));
        })

        return Promise.all(promises)
    }

    public sendEditedMessage(channel: TextChannel, begin: string, edited: string): Promise<any> {
        return this.sendMessageToChannel(channel, begin).then((message: Message) => {
            return message.edit('', this.getEmbedTemplate(edited))
        })
    }

    private getEmbedTemplate(msg: string) {
        return {
            embed: {
                color: 3447003,
                description: msg
            }
        };
    }
}
