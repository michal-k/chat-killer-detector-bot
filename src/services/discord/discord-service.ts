import {inject, injectable} from "inversify";
import {TYPES} from "../../types";
import {Client, Collection, Guild, Message, Snowflake, User} from "discord.js";

@injectable()
export class DiscordService {
    private readonly token: string;
    private client: Client;

    constructor(
        @inject(TYPES.Token) token: string,
        @inject(TYPES.Client)  client: Client,
    ) {
        this.token = token;
        this.client = client;
    }

    public login(): Promise<string> {
        return this.client.login(this.token)
    }

    public findUser(id: string): User {
        return this.client.users.find(user => user.id === id);
    }

    public on(event: string, listener: (message: Message) => Promise<void>) {
        this.client.on(event, listener);
    }

    public getBotUser(): User {
        return this.client.user;
    }

    public getCurrentGuilds(): Collection<Snowflake, Guild> {
        return this.client.guilds;
    }
}
