import {inject, injectable} from "inversify";
import "reflect-metadata";
import {TYPES} from "../types";
import {IKillCountRepository} from "../repositories/kill-count-repository";
import {IKillCountModel} from "../models/kill-count-model";
import * as mongoose from "mongoose";

@injectable()
export class KillCountService {
    private repository: IKillCountRepository;

    constructor(
        @inject(TYPES.KillCountRepository) repository: IKillCountRepository
    ) {
        this.repository = repository;
    }

    /**
     * @param {String} userId
     * @param {Date} date
     * @returns {Promise}
     */
    public increaseKillCount(serverId: string, userId: string, date: Date) {
        console.log('inside')
        return this.repository.updateOne({
            serverId: serverId,
            userId: userId,
            month: date.getMonth() + 1,
            year: date.getFullYear(),

        }, {$inc: {'count': 1}}, {upsert: true}).exec().then(() => {
            console.log('after')
        });
    }

    /**
     * @param today
     * @returns {Promise}
     */
    getCountsForMonth(serverId: string, today: Date): Promise<IKillCountModel[]> {
        return this.repository.find({
            serverId: serverId,
            month: today.getMonth() + 1,
            year: today.getFullYear()
        }).sort({count: 'desc'}).exec()
    }
}
