import moment = require("moment");
import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {TimeLimitProvider} from "../providers/time-limit-provider";

@injectable()
export class KillTimeChecker {

    private timeLimitProvider: TimeLimitProvider;

    private tooBigDiff = 90;

    constructor(
        @inject(TYPES.TimeLimitProvider) timeLimitProvider: TimeLimitProvider
    ) {
        this.timeLimitProvider = timeLimitProvider;
    }

    public isKillingTime(serverId: string, createdAt: Date, now?: Date): Promise<boolean> {
        if (!now) {
            now = new Date(Date.now())
        }

        let duration = moment.duration(moment(now).diff(createdAt));
        let diff = duration.asMinutes();

        return this.timeLimitProvider.getTimeLimit(serverId, now).then((timelimit: number) => {
            if (timelimit < 0) {
                console.log('Not killing because not functioning now.')
                return Promise.resolve(false)
            }

            return this.timeLimitProvider.getTimeLimit(serverId, createdAt).then((timeLimit) => {
                if (timeLimit < 0) {
                    console.log('Not killing because not functioning  when the message was posted.')
                    return Promise.resolve(false)
                }

                if (diff < timeLimit) {
                    console.log(`Not a killing time because: diff=${diff}, timeLimit=${timeLimit}`)
                    return false;
                }

                if (diff > this.tooBigDiff) {
                    console.log(`Not a killing time because: diff=${diff} is too big, timeLimit=${timeLimit}`)
                    return false;
                }
                return true;
            });
        });

    }
}
