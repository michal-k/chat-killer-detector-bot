import {inject, injectable} from "inversify";
import {GuildsProvider} from "../providers/guilds-provider";
import {TYPES} from "../types";
import {Guild, GuildChannel, Message, TextChannel} from "discord.js";
import {KillingMessageController} from "./messages/killing-message-controller";

let schedule = require('node-schedule');

@injectable()
export class KillingMessagePeriodicChecker {

    private guildsProvider: GuildsProvider
    private cron: string;
    private channelNames: string[];
    private killingMessageController: KillingMessageController;

    constructor(
        @inject(TYPES.GuildsProvider)  guildsProvider: GuildsProvider,
        @inject(TYPES.KillingMessageController)  killingMessageController: KillingMessageController,
        @inject(TYPES.Cron)  cron: string,
        @inject(TYPES.ChannelNames)  channelNames: string
    ) {
        this.guildsProvider = guildsProvider;
        this.killingMessageController = killingMessageController;
        this.cron = cron;
        this.channelNames = channelNames.split(',');
    }

    public start() {
        let guilds = this.guildsProvider.getGuilds();
        var j = schedule.scheduleJob(this.cron, () => {
            console.log('The answer to life, the universe, and everything!');
            guilds.forEach((guild: Guild) => {
                console.log(guild.name)
                let channel: TextChannel;
                let found = guild.channels.find((guildChannel: GuildChannel) => this.channelNames.indexOf(guildChannel.name) >= 0);
                if (found && found.type == 'text') {
                    channel = found as TextChannel;
                    console.log("Found channel", channel.name)
                }

                if (!!!channel) {
                    console.log('Channel not found');
                    return;
                }

                channel.fetchMessage(channel.lastMessageID).then((message: Message) => {
                    console.log('lastMessage', message.content)
                    this.killingMessageController.handle(message);
                });

            })
        });
    }
}
