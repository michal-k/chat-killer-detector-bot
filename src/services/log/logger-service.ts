import {Logger} from "winston";
import {inject, injectable} from "inversify";
import {TYPES} from "../../types";

@injectable()
export class LoggerService {
    private logger: Logger;

    constructor(
        @inject(TYPES.Logger) logger: Logger
    ) {
        this.logger = logger;
    }


    public info(message: string) {
        this.logger.info(message)
    }
}
