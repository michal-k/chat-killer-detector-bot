import {GuildChannel, Message, TextChannel} from "discord.js";
import {KillingMessageResolver} from "./killing-message-resolver";
import {inject, injectable} from "inversify";
import {MessagesSender} from "./messages-sender";
import {MessagesDataProvider} from "./messages-data-provider";
import {TYPES} from "../../types";
import {KillCountService} from "../kill-count-service";

@injectable()
export class KillingMessageController {

    private killingMessageFinder: KillingMessageResolver;
    private killService: KillCountService;
    private messagesSender: MessagesSender;
    private dataProvider: MessagesDataProvider;

    constructor(
        @inject(TYPES.KillingMessageResolver) killingMessageFinder: KillingMessageResolver,
        @inject(TYPES.KillCountService) killService: KillCountService,
        @inject(TYPES.MessagesSender) messagesSender: MessagesSender,
        @inject(TYPES.MessagesDataProvider)dataProvider: MessagesDataProvider
    ) {
        this.killingMessageFinder = killingMessageFinder;
        this.killService = killService;
        this.messagesSender = messagesSender;
        this.dataProvider = dataProvider;
    }

    public async handle(message: Message) {

        if (!await this.killingMessageFinder.isKillingMessage(message.guild.id, message)) {
            console.log(`Not a killing message: ${message.content}(${message.createdAt})`)
            return;
        }
        let channel = message.channel;
        console.log("Is killing message!")

        let user = message.author;
        let k = await this.killService.increaseKillCount(message.guild.id, user.id, new Date());
        console.log("After:increaseKillCount")

        await this.messagesSender.sendKillingMessage(message, user);

        let killsWithMembers = await this.sendTop3ForChannel(channel as GuildChannel);

        let usersToNotify = this.dataProvider.getUsersToNotify()
        console.log("After:getUsersToNotify")

        this.messagesSender.sendKillingDmMessages(usersToNotify, user, killsWithMembers);
        console.log("After:sendKillingDmMessages")

    }

    public async sendTop3ForChannel(channel: GuildChannel) {
        console.log("After:sendKillingMessage")
        let kills = await this.killService.getCountsForMonth(channel.guild.id, new Date());

        console.log("After:getCountsForMonth")
        let killsWithMembers = this.dataProvider.getKillsWithMembers(channel.guild, kills);

        console.log("After:getKillsWithMembers")

        let top3Data = this.dataProvider.getTop3Data(killsWithMembers);
        console.log("After:getTop3Data")

        this.messagesSender.sendTop3Message(channel as TextChannel, top3Data);
        console.log("After:sendTop3Message")
        return killsWithMembers;
    }
}


