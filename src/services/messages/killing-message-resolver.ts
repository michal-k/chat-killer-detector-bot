import {Client, Message, User} from "discord.js";
import {KillTimeChecker} from "../kill-time-checker";
import {inject, injectable} from "inversify";
import {TYPES} from "../../types";
import {DiscordService} from "../discord/discord-service";

@injectable()
export class KillingMessageResolver {

    private selfTrigger: boolean;
    private discord: DiscordService;
    private killResolver: KillTimeChecker;

    constructor(
        @inject(TYPES.DiscordService)  discordService: DiscordService,
        @inject(TYPES.KillTimeChecker) killResolver: KillTimeChecker,
        @inject(TYPES.SelfTrigger) selfTrigger: boolean) {
        this.selfTrigger = selfTrigger;
        this.discord = discordService;
        this.killResolver = killResolver;
    }

    public isKillingMessage(serverId: string, message: Message): Promise<boolean> {
        if (this.isThisBotMessage(message)) {
            if (!this.selfTrigger) {
                console.log(`Not a killing time because: is bot message and selftrigger = false.`)
                return Promise.resolve(false);
            }
        }

        if (this.isOtherBot(message)) {
            console.log(`Not a killing time because: is other bot.`)
            return Promise.resolve(false);
        }

        return this.killResolver.isKillingTime(serverId, message.createdAt);
    }

    private isOtherBot(message: Message) {
        return message.author.bot === true;
    }

    private isThisBotMessage(lastMessage: Message) {
        return this.discord.getBotUser().id === lastMessage.author.id;
    }
}
