import {Client, Guild} from "discord.js";
import {inject, injectable} from "inversify";
import {TYPES} from "../../types";
import {KillDataWithMember} from "../../structs/kill-data-with-member";
import {IKillCountModel} from "../../models/kill-count-model";

@injectable()
export class KillsWithMembersProvider {

    private client: Client;

    constructor(
        @inject(TYPES.Client) client: Client
    ) {
        this.client = client;
    }

    public getKillsWithMembers(guild: Guild, kills: IKillCountModel[]): KillDataWithMember[] {
        let killsWithMembers = [];
        for (let i = 0; i < kills.length; i++) {
            let kill = kills[i]
            let member = guild.members.find(member => member.user.id === kill.userId)
            if (!!!member) {
                console.log(`Member: ${kill.userId} not found in guild: ${guild.name}`);
                continue;
            }
            if (!!!member.user)
            {
                continue;
            }
            let toAdd = new KillDataWithMember(kill, member);
            killsWithMembers.push(toAdd);
        }
        return killsWithMembers
    }
}
