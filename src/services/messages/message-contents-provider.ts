import {TYPES} from "../../types";
import {KillDataWithMember} from "../../structs/kill-data-with-member";
import {inject, injectable} from "inversify";
import {User} from "discord.js";

@injectable()
export class MessageContentsProvider {

    private readonly mention: boolean;

    constructor(
        @inject(TYPES.Mention) mention: boolean
    ) {
        this.mention = mention
    }

    /**
     * @param user
     */
    public getKillingMessage(user: User): string {
        let userNameString = this.getMentionString(user);
        return `Gratulacje, ${userNameString}! Udało Ci się zabić czat :)`
    }

    /**
     * @param user
     * @param mention
     */
    public getMentionString(user: User, mention?: boolean) {
        if (this.getMention(mention)) {
            return '<@!' + user.id + '>';
        }
        return '@' + user.username;
    }

    /**
     * @param userWhoKilled
     * @param killsData
     */
    public getDirectMessageUponKill(userWhoKilled: User, killsData: KillDataWithMember[]): string {

        let mentionString = this.getMentionString(userWhoKilled);

        let msg = `Użytkownik ${mentionString} zabił czat.\n\nZabójstwa w tym miesiącu: \n`;

        for (let i = 0; i < killsData.length; i++) {
            let killData = killsData[i];
            let nickname = killData.member.nickname || killData.member.user.username
            let kill = killData.killData
            let userNameString = this.getMentionString(killData.member.user);
            let toAdd = `${userNameString} (${nickname}):  ${kill.count} zabójstw.\n`;

            msg += toAdd;
        }
        return msg;
    }

    public getTop3InfoMessage(top3Data: KillDataWithMember[]): string {
        let msg = '';
        for (let i = 0; i < top3Data.length; i++) {
            let element = top3Data[i];
            let kill = element.killData;
            let userNameString = this.getMentionString(element.member.user);
            let toAdd = `${userNameString} — ${kill.count}\n`;

            msg += toAdd;
        }

        return msg;
    }

    public getTop3MessageHeader(): string {
        return "Najlepsza trójka zabójców w tym miesiącu: \n";
    }

    /**
     * @param mention
     */
    private getMention(mention?: boolean) {
        if (mention === undefined) {
            return this.mention
        }
        return mention;
    }
}
