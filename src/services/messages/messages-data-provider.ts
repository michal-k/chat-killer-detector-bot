import {KillsWithMembersProvider} from "./kills-with-members-provider";
import {Client, Guild, User} from "discord.js";
import {inject, injectable} from "inversify";
import {DiscordService} from "../discord/discord-service";
import {KillDataWithMember} from "../../structs/kill-data-with-member";
import {TYPES} from "../../types";
import {IKillCountModel} from "../../models/kill-count-model";

@injectable()
export class MessagesDataProvider {

    private notifyUsersString: string;
    private killsWithMembersProvider: KillsWithMembersProvider;
    private discord: DiscordService;

    constructor(
        @inject(TYPES.NotifyUsers) notifyUsersString: string,
        @inject(TYPES.DiscordService) discord: DiscordService,
        @inject(TYPES.KillsWithMembersProvider) killsWithMembersProvider: KillsWithMembersProvider
    ) {
        this.discord = discord;
        this.notifyUsersString = notifyUsersString;
        this.killsWithMembersProvider = killsWithMembersProvider;
    }

    public getUsersToNotify(): User[] {
        let ids = this.notifyUsersString.split(',');
        let usersToNotify = [];
        ids.forEach((id) => {
            let userToNotify = this.discord.findUser(id);
            if (userToNotify) {
                usersToNotify.push(userToNotify)
            }
        })
        return usersToNotify
    }

    public getTop3Data(killsWithMembers: KillDataWithMember[]): KillDataWithMember[] {
        return killsWithMembers.slice(0, 3);
    }

    public getKillsWithMembers(guild: Guild, kills: IKillCountModel[]): KillDataWithMember[] {
        return this.killsWithMembersProvider.getKillsWithMembers(guild, kills);
    }
}



