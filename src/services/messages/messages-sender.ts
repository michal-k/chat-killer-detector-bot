import {Message, TextChannel, User} from "discord.js";
import {TYPES} from "../../types";
import {KillDataWithMember} from "../../structs/kill-data-with-member";
import {inject, injectable} from "inversify";
import {DiscordMessageSender} from "../discord/discord-message-sender";
import {MessageContentsProvider} from "./message-contents-provider";

@injectable()
export class MessagesSender {

    private messagesSender: DiscordMessageSender;
    private messages: MessageContentsProvider;

    constructor(
        @inject(TYPES.DiscordMessageSender) messagesSender: DiscordMessageSender,
        @inject(TYPES.MessageContentsProvider) messages: MessageContentsProvider
    ) {
        this.messagesSender = messagesSender;
        this.messages = messages
    }

    public sendKillingMessage(message: Message, userToMention: User) {
        return this.messagesSender.sendMessageToChannel(message.channel as TextChannel, this.messages.getKillingMessage(userToMention), false)
    }

    public sendTop3Message(channel: TextChannel, top3Data: KillDataWithMember[]) {
        return this.messagesSender.sendMessageToChannel(
            channel,
            this.messages.getTop3MessageHeader()
            + '\n'
            + this.messages.getTop3InfoMessage(top3Data)
        );
    }

    public sendKillingDmMessages(users: User[], userWhoKilled: User, killData: KillDataWithMember[]) {
        return this.messagesSender.sendMessageToMultipleUsers(users,
            this.messages.getDirectMessageUponKill(userWhoKilled, killData)
        );
    }
}
