import {inject, injectable} from "inversify";
import {TYPES} from "../types";
import {Message} from "discord.js";

@injectable()
export class PermissionsService {
    private readonly overrideUserId: string;

    constructor(
        @inject(TYPES.OverrideUserId) overrideUserId: string
    ) {
        this.overrideUserId = overrideUserId
    }


    public isOwnerOrOverride(message: Message) {
        if (message.author.id === this.overrideUserId) {
            return true
        }

        return message.author.id === message.guild.owner.id;
    }
}
