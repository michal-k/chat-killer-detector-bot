import {IKillCount} from "../entities/kill-count";
import {GuildMember} from "discord.js";

export class KillDataWithMember {
    public killData: IKillCount
    public member: GuildMember;

    constructor(data: IKillCount, member: GuildMember) {
        this.killData = data;
        this.member = member;

        // if (!_.get(this.member, 'user.id')) {
        //     throw new Error(`User id=${this.member.displayName.id} is different than in DB=${this.killData.userId}!`);
        // }
        //
        // if (this.killData.userId !== this.member.user.id) {
        //     throw new Error(`User id=${this.member.user.id} is different than in DB=${this.killData.userId}!`);
        // }
    }
}
