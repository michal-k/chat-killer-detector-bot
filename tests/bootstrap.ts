require('dotenv').config()

import "reflect-metadata";
import * as mongoose from "mongoose";
import {expect} from 'chai';
import 'mocha';

let async = require('async');
let fixtures = require('node-mongoose-fixtures');

fixtures.reset = function (modelName, db, callback) {
    if (modelName instanceof mongoose.Mongoose) {
        callback = db;
        db = modelName;
        modelName = null;
    }
    if (typeof modelName === 'function') {
        callback = modelName;
        modelName = null;
        db = mongoose;
    }
    if (typeof db === 'function') {
        callback = db, db = mongoose;
    }
    if (typeof modelName === 'undefined') {
        db = mongoose;
        callback = function () {
        };   // No operation
    }

    var deleteModel = function (modelName, done) {
        db.model(modelName).deleteMany(done);
    };

    if (modelName) {
        deleteModel(modelName, callback);
    } else {
        async.map(db.modelNames(), deleteModel, callback);
    }
};


