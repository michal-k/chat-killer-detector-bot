import 'mocha';
import {expect} from 'chai';
import container from "../../inversify.config";
import {TYPES} from "../../src/types";
import {DiscordService} from "../../src/services/discord/discord-service";
import {instance, mock} from "ts-mockito";
import {ChatKillerDetectorBot} from "../../src/chat-killer-detector-bot";

describe('ChatKillerDetectorBot', () => {
    let discordMock: DiscordService;
    let discordInstance: DiscordService;
    let bot: ChatKillerDetectorBot;

    beforeEach(() => {
        discordMock = mock(DiscordService);
        discordInstance = instance(discordMock);

        container.rebind<DiscordService>(TYPES.DiscordService).toConstantValue(discordInstance);
        bot = container.get<ChatKillerDetectorBot>(TYPES.ChatKillerDetectorBot);
    })

    it('should be created', () => {
        expect(bot).to.be.not.null
    });
});
