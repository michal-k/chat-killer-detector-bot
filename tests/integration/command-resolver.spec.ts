import 'mocha';
import {expect} from 'chai';
import container from "../../inversify.config";
import {TYPES} from "../../src/types";
import {DiscordService} from "../../src/services/discord/discord-service";
import {CommandResolver} from "../../src/commands/resolver/command-resolver";
import {instance, mock} from "ts-mockito";

describe('CommandResolver', () => {
    let discordMock: DiscordService;
    let discordInstance: DiscordService;
    let resolver: CommandResolver;

    beforeEach(() => {
        discordMock = mock(DiscordService);
        discordInstance = instance(discordMock);

        container.rebind<DiscordService>(TYPES.DiscordService).toConstantValue(discordInstance);
        resolver = container.get<CommandResolver>(TYPES.CommandResolver);
    })

    it('should be created', () => {
        expect(resolver).to.be.not.null
    });
});
