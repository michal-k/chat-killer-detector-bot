import {HelpCommand} from "../../../src/commands/help-command";
import {DiscordService} from "../../../src/services/discord/discord-service";
import container from "../../../inversify.config";
import {TYPES} from "../../../src/types";
import {anything, instance, mock, when} from "ts-mockito";
import {expect} from "chai";
import {EJSON} from "bson";
import {Message} from "discord.js";
import {DiscordMessageSender} from "../../../src/services/discord/discord-message-sender";
import {CommandInterface} from "../../../src/commands/interfaces/command-interface";

describe('HelpCommand', () => {
    let discordMock: DiscordService, messagesSenderMock: DiscordMessageSender;
    let discordInstance: DiscordService, messagesSenderInstance: DiscordMessageSender;
    let command: HelpCommand;
    let msg;
    beforeEach(() => {
        discordMock = mock(DiscordService);
        discordInstance = instance(discordMock);
        messagesSenderMock = mock(DiscordMessageSender);
        messagesSenderInstance = instance(messagesSenderMock);
        when(messagesSenderMock.sendMessageToChannel(anything(), anything())).thenCall((channel, message) => {
            console.log('call');
            msg = message;
        })
        command = new HelpCommand(
            container.get<CommandInterface[]>(TYPES.CommandsForHelp),
            messagesSenderInstance
        )
    })

    it('should display help', async () => {
        await command.run(new Message(null, null, null), '');
        expect(msg.split('\n')).to.deep.eq([
            "Lista komend:",
            "list-times - Wyświetlanie czasów, po jakich bot się uaktywnia.",
            "set-time - Ustawienie czasu działania bota.",
            "set-setting - Ustawienie parametrów serwera.",
            "list-settings - Wyświetlanie ustawień serwera.",
            "help - Wyświetlenie pomocy.",
            "Użyj help <nazwa_komendy> aby uzyskać więcej informacji.",
            ""
        ])
    });

    it('should display help for command', async () => {
        await command.run(new Message(null, null, null), 'set-setting');
        expect(msg.split('\n')).to.deep.equal([
            "set-setting - Ustawienie parametrów serwera.",
            "Użycie: set-setting <nazwa> <wartość>",
            "Przykładowe wywołania:",
            "defaultTime 30",
            "prefix +pr",
            "setAllowedRoleIds '1,2,3'",
            // "channelNames 'a,b,c'",
            // "notifyUsersIds '1,2,3'",
            ""
        ])
    });
});
