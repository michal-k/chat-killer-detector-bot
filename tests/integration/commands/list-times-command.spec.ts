import container from "../../../inversify.config";
import {TYPES} from "../../../src/types";
import {anything, when} from "ts-mockito";
import {expect} from "chai";
import {Guild, Message} from "discord.js";
import {DiscordMessageSender} from "../../../src/services/discord/discord-message-sender";
import {ListTimesCommand} from "../../../src/commands/list-times-command";
import {PermissionsService} from "../../../src/services/permissions-service";
import {DefaultTimeLimitProvider} from "../../../src/providers/default-time-limit-provider";
import {getMock, MockContainer} from "../../shared/mock-container";
import {applyFixturesAndHooks} from "../services/mongodb-with-fixtures";
import {IServerTimeLimitRepository} from "../../../src/repositories/server-time-limit-repository";

describe('ListTimesCommand', () => {

    let messagesSenderMock: MockContainer<DiscordMessageSender>;
    let command: ListTimesCommand;
    let msg;

    let merged = Object.assign(
        require('../../fixtures/server-settings.json'),
        require('../../fixtures/time-limits.json')
    );
    applyFixturesAndHooks(merged, () => {
        messagesSenderMock = getMock<DiscordMessageSender>(DiscordMessageSender);
        when(messagesSenderMock.mock.sendMessageToChannel(anything(), anything())).thenCall((channel, message) => {
            console.log('call');
            msg = message;
        })
        command = new ListTimesCommand(
            container.get<PermissionsService>(TYPES.PermissionsService),
            messagesSenderMock.instance,
            container.get<IServerTimeLimitRepository>(TYPES.ServerTimeLimitRepository),
            container.get<DefaultTimeLimitProvider>(TYPES.DefaultTimeLimitProvider),
        )
        command.setTimezoneOffset(60);
    })

    it('should display times', async () => {
        let message = getMock<Message>(Message);
        let guild = getMock<Guild>(Guild);
        when(message.mock.guild).thenReturn(guild.instance);
        when(guild.mock.id).thenReturn('aaa');
        await command.run(message.instance, '').catch(() => {
            expect.fail('fail')
        })
        expect(msg.split('\n')).to.deep.eq([
            "Oto czasy działania bota (domyślny czas: 10 min):",
            " Godz. 0:00-0:59 – 10 min",
            " Godz. 1:00-1:59 – 10 min",
            " Godz. 2:00-2:59 – 10 min",
            " Godz. 3:00-3:59 – 10 min",
            " Godz. 4:00-4:59 – 10 min",
            " Godz. 5:00-5:59 – 10 min",
            " Godz. 6:00-6:59 – 10 min",
            " Godz. 7:00-7:59 – 10 min",
            " Godz. 8:00-8:59 – 10 min",
            " Godz. 9:00-9:59 – 10 min",
            " Godz. 10:00-10:59 – 10 min",
            " Godz. 11:00-11:59 – 10 min",
            " Godz. 12:00-12:59 – 15 min",
            " Godz. 13:00-13:59 – 10 min",
            " Godz. 14:00-14:59 – 30 min",
            " Godz. 15:00-15:59 – 10 min",
            " Godz. 16:00-16:59 – 50 min",
            " Godz. 17:00-17:59 – 10 min",
            " Godz. 18:00-18:59 – 10 min",
            " Godz. 19:00-19:59 – 10 min",
            " Godz. 20:00-20:59 – 10 min",
            " Godz. 21:00-21:59 – 10 min",
            " Godz. 22:00-22:59 – 10 min",
            " Godz. 23:00-23:59 – 10 min",
            "Dodatkowo, bot sprawdza czas co kilka minut, więc powyższe czasy mogą być minimalnie wyższe."
        ])
    });
});
