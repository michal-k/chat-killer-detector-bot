import {expect} from 'chai';
import 'mocha';
import container from "../../../inversify.config";
import {TYPES} from "../../../src/types";
import {IKillCountRepository} from "../../../src/repositories/kill-count-repository";
import {KillCountService} from "../../../src/services/kill-count-service";
import {IKillCountModel} from "../../../src/models/kill-count-model";
import {applyFixturesAndHooks, expectRecordsCount} from "./mongodb-with-fixtures";
import {createDate} from "../../shared/date-helper";

describe('KillCountRepository', () => {
    const serverA = "aaa";
    const serverB = "bbb";

    let service: KillCountService;
    let repository: IKillCountRepository;

    applyFixturesAndHooks(require('../../fixtures/kill-counts.json'), () => {
        repository = container.get<IKillCountRepository>(TYPES.KillCountRepository);
        service = container.get<KillCountService>(TYPES.KillCountService);
    })

    it('should increase an existing entry', async () => {
        await service.increaseKillCount(serverA, '1', createDate("2019-1-20"));
        let result: IKillCountModel = await repository.findOne({serverId: serverA, userId: '1', month: 1, year: 2019});
        expect(result.count).to.equal(11);
        return expectRecordsCount(repository, {}, 4);
    });

    it('should create a new entry', async () => {
        let before = await repository.find({})
        console.log('before count', before.length)
        await service.increaseKillCount(serverA, '100', createDate("2019-3-20"));
        let result = await repository.findOne({serverId: serverA, userId: '100', month: 3, year: 2019});
        expect(result.count).to.equal(1);
        return expectRecordsCount(repository, {}, 5);
    });

    it('should create a new entry for serverB', async () => {
        await service.increaseKillCount(serverB, '100', createDate("2019-3-20"));
        let result = await repository.findOne({serverId: serverB, userId: '100', month: 3, year: 2019});
        expect(result.count).to.equal(1);
        return expectRecordsCount(repository, {}, 5);
    });

    it('get counts for january', async () => {
        let result = await service.getCountsForMonth(serverA, createDate("2019-1-20"));
        expect(result.length).to.equal(2);
        expect(result[0]).to.contain({
            serverId: serverA,
            userId: '1',
            count: 10,
            month: 1,
            year: 2019
        });

        expect(result[1]).to.contain({
            serverId: serverA,
            userId: '2',
            count: 5,
            month: 1,
            year: 2019,
        });
    });

    it('get counts for january for serverB', async () => {
        let result = await service.getCountsForMonth(serverB, createDate("2019-1-20"));
        expect(result.length).to.equal(1);
        expect(result[0]).to.contain({
            serverId: serverB,
            userId: '1',
            count: 10,
            month: 1,
            year: 2019
        });
    });
});
