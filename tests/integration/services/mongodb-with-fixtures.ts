import container from "../../../inversify.config";
import {TYPES} from "../../../src/types";
import {expect} from "chai";
import {Model, MongooseDocument} from "mongoose";

export function applyFixturesAndHooks(fixturesJson: string, beforeEachCallback: CallableFunction) {

    let fixtures = require('node-mongoose-fixtures');
    let mongoose = require('mongoose');

    beforeEach((done) => {
        beforeEachCallback();
        fixtures(fixturesJson, mongoose, () => {
            done()
        })
    })

    before((done) => {
        let testUri = container.get(TYPES.MongodbTestUri);
        mongoose.connect(testUri, {useNewUrlParser: true}).then(() => {
            done();
        })
    })

    after((done) => {
        mongoose.connection.close().then(() => {
            done();
        })
    })

    afterEach((done) => {
        fixtures.reset(() => {
            done();
        })
    })
}

export async function expectRecordsCount(repository: Model<any>, search: object, count: number) {
    let all = await repository.find(search);
    return expect(all.length).to.equal(count);
}
