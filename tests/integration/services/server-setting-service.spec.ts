import {expect} from 'chai';
import 'mocha';
import container from "../../../inversify.config";
import {TYPES} from "../../../src/types";
import {IServerSettingRepository} from "../../../src/repositories/server-setting-repository";
import {IServerSettingModel} from "../../../src/models/server-setting-model";
import {applyFixturesAndHooks, expectRecordsCount} from "./mongodb-with-fixtures";

describe('ServerSettingsService', () => {
    const serverA = "aaa";
    const serverB = "bbb";
    const serverC = "ccc";

    let repository: IServerSettingRepository;

    applyFixturesAndHooks(require('../../fixtures/server-settings.json'), () => {
        repository = container.get<IServerSettingRepository>(TYPES.ServerSettingRepository);
    })

    it('should increase an existing entry', async () => {
        await repository.setSettings(serverA, {'prefix': '+xy'});
        let result: IServerSettingModel = await repository.findOne({serverId: serverA});
        expect(result.prefix).to.equal('+xy');
        return expectRecordsCount(repository, {}, 2);
    });

    it('should create a new entry', async () => {
        await repository.setSettings(serverC, {'prefix': '+gy'});
        let result: IServerSettingModel = await repository.findOne({serverId: serverC});
        expect(result.prefix).to.equal('+gy');
        return expectRecordsCount(repository, {}, 3);
    });

    it('get settings for serverA', async () => {
        let result = await repository.getSettings(serverA);
        expect(result.defaultTime).to.equal('10');
        expect(result.prefix).to.equal("+pr");
        expect(result.setAllowedRoleIds).to.equal("a,b,c");
        expect(result.channelNames).to.equal("chA,chB,chC");
        expect(result.notifyUsersIds).to.equal("u1,u2,u3");
    });

    it('get incomplete settings serverB', async () => {
        let result = await repository.getSettings(serverB);
        expect(result.defaultTime).to.equal('10');
        expect(result.prefix).to.equal("+pr");
        expect(result.setAllowedRoleIds).to.be.undefined
        expect(result.channelNames).to.be.undefined
        expect(result.notifyUsersIds).to.be.undefined
    });
});
