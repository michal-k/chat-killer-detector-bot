import {expect} from 'chai';
import 'mocha';
import container from "../../../inversify.config";
import {TYPES} from "../../../src/types";
import {IServerTimeLimitRepository} from "../../../src/repositories/server-time-limit-repository";
import {applyFixturesAndHooks, expectRecordsCount} from "./mongodb-with-fixtures";

describe('TimeLimitRepository', () => {
    const serverA = "aaa";
    const serverB = "bbb";

    let repository: IServerTimeLimitRepository;

    applyFixturesAndHooks(require('../../fixtures/time-limits.json'), () => {
        repository = container.get<IServerTimeLimitRepository>(TYPES.ServerTimeLimitRepository);
    })

    it('should increase an existing entry', async () => {
        await repository.setTimeLimit(serverA, 55, 10);
        let result = await repository.findOne({serverId: serverA, utcHour: 10});
        expect(result.timeLimit).to.equal(55);
        return expectRecordsCount(repository, {serverId: serverA}, 3);
    });

    it('should create a new entry', async () => {
        await repository.setTimeLimit(serverA, 25, 16);
        let result = await repository.findOne({serverId: serverA, utcHour: 16});
        expect(result.timeLimit).to.equal(25);
        return expectRecordsCount(repository, {serverId: serverA}, 4);
    });

    it('should create a new entry for serverB', async () => {
        await repository.setTimeLimit(serverB, 25, 16);
        let result = await repository.findOne({serverId: serverB, utcHour: 16});
        expect(result.timeLimit).to.equal(25);
        return expectRecordsCount(repository, {serverId: serverB}, 2);
    });

    it('get timeLimit for serverA', async () => {
        let result = await repository.getTimeLimitForUtcHour(serverA, 14);
        expect(result.timeLimit).to.equal(50);
    });

    it('get timeLimit for empty', async () => {
        let result = await repository.getTimeLimitForUtcHour(serverA, 22);
        expect(result).to.be.null
    });

    it('get counts for serverA', async () => {

        let result = await repository.getUTCTimeLimits(serverA);
        expect(result.length).to.equal(3);
        expect(result[0]).to.contain({
            serverId: serverA,
            utcHour: 10,
            timeLimit: 15
        });

        expect(result[1]).to.contain({
            serverId: serverA,
            utcHour: 12,
            timeLimit: 30
        });

        expect(result[2]).to.contain({
            serverId: serverA,
            utcHour: 14,
            timeLimit: 50
        });
    });

    it('get counts for january for serverB', async () => {
        let result = await repository.getUTCTimeLimits(serverB);
        expect(result.length).to.equal(1);
        expect(result[0]).to.contain({
            serverId: serverB,
            utcHour: 10,
            timeLimit: 25
        });
    });
});
