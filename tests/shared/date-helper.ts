export function createDate(dateString: string) {
    return new Date(Date.parse(dateString));
}
