import {instance, mock} from "ts-mockito";
import {Mock} from "moq.ts";

export class MockContainer<T> {
    public readonly mock: T;
    public readonly instance: T;

    constructor(mock: T, instance: T) {
        this.mock = mock;
        this.instance = instance;
    }
}

export function getMock<T>(clazz: { new(...args: any[]): T } | (Function & { prototype: T })): MockContainer<T> {
    let mockedClass: T = mock(clazz);
    let mockedInstance: T = instance(mockedClass);
    return new MockContainer<T>(mockedClass, mockedInstance);
}

export class Mockd<T> {
    public readonly mock: Mock<T>;

    // public readonly instance: T;

    constructor(mock: Mock<T>) {
        this.mock = mock;
        // this.instance = instance;
    }

    instance(): T {
        return this.mock.object();
    }

    static getMock<T>(mockId?: string): Mockd<T> {
        let mockedClass: Mock<T> = new Mock(mockId);
        return new Mockd<T>(mockedClass);
    }
}

