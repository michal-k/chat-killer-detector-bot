import 'mocha';
import {expect} from 'chai';
import {anything, instance, mock, verify, when} from "ts-mockito";
import {GuildsProvider} from "../../../src/providers/guilds-provider";
import {DiscordService} from "../../../src/services/discord/discord-service";
import {Collection, Guild, Snowflake} from "discord.js";
import {LoggerService} from "../../../src/services/log/logger-service";


describe('GuildsProvider', () => {
    const testGuild1 = 'test1';
    const testGuild2 = 'test2';
    const testGuild3 = 'test3';
    const testGuildsString = `${testGuild1},${testGuild2},${testGuild3}`;
    const prodGuild1 = 'prod1';
    const prodGuild2 = 'prod2';
    const prodGuildsString = `${prodGuild1},${prodGuild2}`;

    let discordMock: DiscordService;
    let discordInstance: DiscordService;
    let loggerMock: LoggerService;
    let loggerInstance: LoggerService;

    beforeEach(() => {
        discordMock = mock(DiscordService);
        discordInstance = instance(discordMock);
        loggerMock = mock(LoggerService);
        loggerInstance = instance(loggerMock);
        when(discordMock.getCurrentGuilds()).thenReturn(new Collection<Snowflake, Guild>([
            createGuildArray(testGuild1),
            createGuildArray(testGuild2),
            createGuildArray(prodGuild1),
            createGuildArray(testGuild3),
            createGuildArray(prodGuild2),
        ]));
    });

    it('should return all test guilds', () => {
        let service = new GuildsProvider(discordInstance, loggerInstance, testGuildsString, "", true);
        let guilds = service.getGuilds();
        expect(guilds.length).to.equal(3);
        expect(guilds[0].id).to.equal(testGuild1);
        expect(guilds[1].id).to.equal(testGuild2);
        expect(guilds[2].id).to.equal(testGuild3);
    })

    it('should return all prod guilds', () => {
        let service = new GuildsProvider(discordInstance, loggerInstance, testGuildsString, testGuildsString, false);
        let guilds = service.getGuilds();
        expect(guilds.length).to.equal(2);
        expect(guilds[0].id).to.equal(prodGuild1);
        expect(guilds[1].id).to.equal(prodGuild2);
    })

    it('should return all guilds', () => {
        let service = new GuildsProvider(discordInstance, loggerInstance, testGuildsString, "", false);
        let guilds = service.getGuilds();
        expect(guilds.length).to.equal(5);
        expect(guilds[0].id).to.equal(testGuild1);
        expect(guilds[1].id).to.equal(testGuild2);
        expect(guilds[2].id).to.equal(prodGuild1);
        expect(guilds[3].id).to.equal(testGuild3);
        expect(guilds[4].id).to.equal(prodGuild2);
    })

    it('should return no guilds', () => {
        let service = new GuildsProvider(discordInstance, loggerInstance, "", "", true);
        let guilds = service.getGuilds();
        expect(guilds.length).to.equal(0);
    })

    let createGuildMock = function (id: string) {
        let guild1 = instance(mock(Guild));
        guild1.id = id;
        return guild1;
    };

    let createGuildArray = function (name: string): [string, Guild] {
        return [name, createGuildMock(name)];
    };
});

