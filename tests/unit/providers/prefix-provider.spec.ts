import 'mocha';
import {expect} from 'chai';
import {anything, instance, mock, verify, when} from "ts-mockito";
import {PrefixProvider} from "../../../src/providers/prefix-provider";
import {getMock, MockContainer, Mockd} from "../../shared/mock-container";
import {Client, ClientUser, Guild, Message} from "discord.js";
import {IServerSettingRepository} from "../../../src/repositories/server-setting-repository";
import {Model} from "mongoose";
import container from "../../../inversify.config";
import {TYPES} from "../../../src/types";
import {Mock} from "moq.ts";


describe('PrefixProvider', () => {

    let provider: PrefixProvider
    let repoMock: MockContainer<IServerSettingRepository>;
    let settingsToReturn = {
        channelNames: "",
        defaultTime: 0,
        notifyUsersIds: "",
        prefix: "",
        setAllowedRoleIds: ""
    };

    let mockdRepo:Mockd<IServerSettingRepository>;

    beforeEach(() => {
        mockdRepo = Mockd.getMock<IServerSettingRepository>();

        mockdRepo.mock
            .setup(instance => instance.getSettings("a"))
            .returns(Promise.resolve(settingsToReturn))
        // repoMock = getMock<IServerSettingRepository>(repositoryReal);

        // when(repoMock.mock.getSettings(anything())).thenReturn(Promise.resolve(settingsToReturn))

        provider = new PrefixProvider(mockdRepo.instance())
    });

    it('should return prefix', async () => {
        settingsToReturn.prefix = '_pr';

        let msg =  Mockd.getMock<Message>();
        let guild = Mockd.getMock<Guild>();
        let client = Mockd.getMock<Client>();
        let user = Mockd.getMock<ClientUser>();

        let instance = msg.instance();
        instance.content = '_pr a'

        // @ts-ignore
        instance.client = client.instance()
        // @ts-ignore
        instance.client.user = user.instance()
        instance.client.user.id = '5'
        // @ts-ignore
        instance.guild = guild.instance()
        instance.guild.id = 'a'

        let prefix = await provider.findPrefixUsed(instance)
        expect(prefix).to.eq('_pr')
        // verify(repoMock.mock.getSettings(anything())).once()
    })

    it('should return default prefix', async () => {
        settingsToReturn.prefix = null;


        let msg = Mockd.getMock<Message>();
        // @ts-ignore
        let instance = msg.instance();
        // @ts-ignore
        instance.guild = Mockd.getMock<Guild>();
        instance.guild.id = "a";
        // @ts-ignore
        instance.content = '!zc a';
        // @ts-ignore
        instance.client = Mockd.getMock<Client>().instance();
        // @ts-ignore
        instance.client.user = Mockd.getMock<ClientUser>().instance();
        // @ts-ignore
        instance.client.user.id = '5'
        let prefix = await provider.findPrefixUsed(instance)
        expect(prefix).to.eq('!zc')
        // verify(repoMock.mock.getSettings(anything())).once()
    })
});

