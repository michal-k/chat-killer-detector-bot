import 'mocha';
import {TextChannel, User} from "discord.js";
import {instance, mock, verify} from "ts-mockito";
import {DiscordMessageSender} from "../../../../src/services/discord/discord-message-sender";

describe('DiscordMessageSender', () => {

    let service: DiscordMessageSender;

    let userMock: User;
    let channelMock: TextChannel;

    beforeEach(() => {
        userMock = mock(User);
        channelMock = mock(TextChannel);

        service = new DiscordMessageSender();
    })


    it('should send message', () => {

        let channel = instance(channelMock)
        service.sendMessageToChannel(channel, 'someMessage', false)
        verify(channelMock.send('someMessage')).once();
    })


    it('should send multiple messages', async () => {

        let userMock1 = mock(User);
        let userMock2 = mock(User);
        let userMock3 = mock(User);

        let user1 = instance(userMock1);
        let user2 = instance(userMock2);
        let user3 = instance(userMock3);

        let users = [user1, user2, user3]
        await service.sendMessageToMultipleUsers(users, 'someMessage')
        verify(userMock1.send('someMessage')).once();
        verify(userMock2.send('someMessage')).once();
        verify(userMock3.send('someMessage')).once();
    })
});

