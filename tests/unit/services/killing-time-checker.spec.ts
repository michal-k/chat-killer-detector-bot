import 'mocha';
import {expect} from 'chai';
import {anything, instance, mock, verify, when} from "ts-mockito";
import {KillTimeChecker} from "../../../src/services/kill-time-checker";
import {TimeLimitProvider} from "../../../src/providers/time-limit-provider";
import {getMock, MockContainer} from "../../shared/mock-container";
import {createDate} from "../../shared/date-helper";


describe('KillTimeChecker', () => {

    let serverId = "abc";
    let service: KillTimeChecker;

    let providerMock: MockContainer<TimeLimitProvider>;

    let messageCreatedAt: Date;
    let now: Date;

    beforeEach(() => {
        now = createDate('2019-01-20 15:25:00');
        messageCreatedAt = createDate('2019-01-20 15:20:00')

        providerMock = getMock<TimeLimitProvider>(TimeLimitProvider);

        service = new KillTimeChecker(providerMock.instance);
    })

    it('should handle as killing message no param', async () => {
        Date.now = () => Date.parse('2019-01-20 15:25:00')

        whenDateThenProvideLimit(messageCreatedAt, 4);
        whenDateThenProvideLimit(anything(), 1);

        expect(await service.isKillingTime(serverId, messageCreatedAt)).to.be.true

        Date.now = () => Date.now()
    })


    it('should handle as no killing message', async () => {
        whenDateThenProvideLimit(messageCreatedAt, 100);
        whenNowActive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.false
    })


    it('should handle as killing message', async () => {

        whenDateThenProvideLimit(messageCreatedAt, 2);
        whenNowActive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.true
    })

    it('should handle as no killing message', async () => {
        whenDateThenProvideLimit(messageCreatedAt, 6);
        whenNowActive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.false
    })

    it('should handle as killing message 61min', async () => {
        messageCreatedAt = createDate('2019-01-20 12:10:00')
        now = createDate('2019-01-20 13:20:00');

        whenDateThenProvideLimit(messageCreatedAt, 61);
        whenNowActive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.true
    })

    it('should handle as no killing message 95min', async () => {
        messageCreatedAt = createDate('2019-01-20 10:10:00')
        now = createDate('2019-01-20 13:20:00');

        whenDateThenProvideLimit(messageCreatedAt, 60);
        whenNowActive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.false
    })

    it('should handle as no killing message because inactive', async () => {
        messageCreatedAt = createDate('2019-01-20 12:20:00')
        now = createDate('2019-01-20 13:20:00');

        whenDateThenProvideLimit(messageCreatedAt, -1);
        whenNowActive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.false
    })

    it('should handle as no killing message because now inactive', async () => {
        messageCreatedAt = createDate('2019-01-20 12:20:00')
        now = createDate('2019-01-20 13:20:00');

        whenDateThenProvideLimit(messageCreatedAt, 5);
        whenNowInactive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.false
    })

    it('should handle as no killing message because now inactive', async () => {
        messageCreatedAt = createDate('2019-01-20 12:20:00')
        now = createDate('2019-01-20 13:20:00');

        whenDateThenProvideLimit(messageCreatedAt, 5);
        whenNowInactive();

        expect(await service.isKillingTime(serverId, messageCreatedAt, now)).to.be.false
    })

    let whenNowActive = function () {
        whenDateThenProvideLimit(now, 0);
    };

    let whenNowInactive = function () {
        whenDateThenProvideLimit(now, -1);
    };

    let whenDateThenProvideLimit = function (date, timeLimit: number) {
        when(providerMock.mock.getTimeLimit(serverId, date)).thenReturn(Promise.resolve<number>(timeLimit))
    };
});

