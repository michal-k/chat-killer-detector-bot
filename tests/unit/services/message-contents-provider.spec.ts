import {KillDataWithMember} from "../../../src/structs/kill-data-with-member";
import {GuildMember, User} from "discord.js";
import {instance, mock} from "ts-mockito";
import {expect} from "chai";
import {MessageContentsProvider} from "../../../src/services/messages/message-contents-provider";

describe('MessageContentsProvider', () => {

    let service: MessageContentsProvider;
    let mockedUser: User;

    beforeEach(() => {
        mockedUser = mock(User);

        service = new MessageContentsProvider(true);
    })

    it('should mention properly', () => {
        let user = instance(mockedUser);
        user.username = 'someUsername';
        user.id = '123';
        expect(service.getMentionString(user)).to.equal('<@!123>');
        expect(service.getMentionString(user, false)).to.equal('@someUsername')
        expect(service.getMentionString(user, true)).to.equal('<@!123>')
    })

    it('should getKillingMessage with mention', () => {
        let user = instance(mockedUser);
        user.id = '123';
        let message = service.getKillingMessage(user);
        expect(message).to.equal('Gratulacje, <@!123>! Udało Ci się zabić czat :)');
    })

    it('should return top3Header message', () => {
        expect(service.getTop3MessageHeader()).to.equal("Najlepsza trójka zabójców w tym miesiącu: \n");
    })

    it('should getTop3InfoMessage with mention', () => {

        let data = createKillData();

        let message = service.getTop3InfoMessage(data);
        let messageArray = message.split("\n");
        expect(messageArray).to.deep.equal([
            "<@!123> — 5",
            "<@!222> — 2",
            "<@!333> — 1",
            "",
        ]);
    })

    it('should getDirectMessageUponKill with mention', () => {
        let user = instance(mockedUser);

        user.username = 'someUsername';
        user.id = '123';
        let data = createKillData();

        let message = service.getDirectMessageUponKill(user, data);
        let messageArray = message.split("\n");
        expect(messageArray).to.deep.equal([
            "Użytkownik <@!123> zabił czat.",
            "",
            "Zabójstwa w tym miesiącu: ",
            "<@!123> (someNickname):  5 zabójstw.",
            "<@!222> (someNickname222):  2 zabójstw.",
            "<@!333> (someNickname333):  1 zabójstw.",
            "",
        ]);
    })

    let createKillData = function () {
        let data = [];
        data.push(new KillDataWithMember({
            serverId: "aaa",
            userId: '123',
            count: 5,
            month: 2,
            year: 2019
        }, getMemberMock('123', 'someUsername', 'someNickname')))
        data.push(new KillDataWithMember({
            serverId: "aaa",
            userId: '222',
            count: 2,
            month: 2,
            year: 2019
        }, getMemberMock('222', 'someUsername222', 'someNickname222')))
        data.push(new KillDataWithMember({
            serverId: "aaa",
            userId: '333',
            count: 1,
            month: 2,
            year: 2019
        }, getMemberMock('333', 'someUsername333', 'someNickname333')))
        return data;
    };

    let getMemberMock = function (id, username, nickname) {
        let mockedGuildMember = mock(GuildMember);
        let member: GuildMember = instance(mockedGuildMember);
        let user = mock(User);
        member.user = instance(user);
        member.user.id = id;
        member.nickname = nickname;
        return member;
    };

});
